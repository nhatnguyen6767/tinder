//
//  Usuario.swift
//  Tinder
//
//  Created by net=0 on 13/8/20.
//  Copyright © 2020 net=0. All rights reserved.
//

import Foundation

struct Usuario {
    let id: Int
    let nome: String
    let idade: Int
    let match: Bool
    let frase: String
    let foto: String
}
